import logo from './logo.svg';
import './App.css';
import DemoClass from "./DemoComponent/DemoClass"
import DemoFunction from "./DemoComponent/DemoFunction"
import Ex_Layout from './Ex_Layout/Ex_Layout';
// import Footer from './Ex_Layout/Footer';

function App() {
  return (
    <div className="App">
      {/* <DemoClass/>
      <DemoFunction></DemoFunction> */}
      <Ex_Layout/>
    </div>
  );
}

export default App;
