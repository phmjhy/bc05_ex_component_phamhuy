import logo from "./logo.svg";
import "./App.css";
import DemoClass from "./DemoComponent/DemoClass";
import DemoFunction from "./DemoComponent/DemoFunction.jsx";
import Ex_Layout from "./Ex_Layout/Ex_Layout";
import DataBinding from "./DataBinding/DataBinding";
import DemoProps from "./DemoProps/DemoProps";
// import RenderWithMap from "./RenderWithMap/RenderWithMap";
function App() {
  return (
    <div className="App">
      {/* <DemoClass /> */}
      {/* <DemoFunction></DemoFunction> */}
      {/* <Ex_Layout /> */}
      {/* <RenderWithMap /> */}
      {/* <DataBinding /> */}
      <DemoProps />
    </div>
  );
}

export default App;
