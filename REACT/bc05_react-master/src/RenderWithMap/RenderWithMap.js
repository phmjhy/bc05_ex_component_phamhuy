import React, { Component } from "react";
import CardItem from "./CardItem";
import { dataMovie } from './dataMovie'

export default class RenderWithMap extends Component {
  renderMovieList = () => {
    return dataMovie.map((item) => {
      return <CardItem/>;
    })
  };
  render() {
    return <div>{this.renderMovieList()}</div>;
  }
}
