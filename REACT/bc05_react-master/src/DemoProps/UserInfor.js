import React, { Component } from "react";

export default class UserInfor extends Component {
  render() {
    console.log(this.props);
    return (
      <div>
        <h2>Username: {this.props.data.name}</h2>
        <h2>Gmail:{this.props.data.gmail}</h2>

        {this.props.renderContent()}
      </div>
    );
  }
}
