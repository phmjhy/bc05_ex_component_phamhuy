import React, { Component } from "react";
import UserInfor from "./UserInfor";
export default class DemoProps extends Component {
  renderTitle = () => {
    return <h1> Nice to meet you</h1>;
  };
  handleClickMe = () => {
    console.log("yes");
  };
  render() {
    let user = {
      name: "Bob",
      gmail: "bob@gmail.com",
    };

    return (
      <div>
        <h2>DemoProps</h2>
        <button onClick={this.handleClickMe} className="btn btn-success">
          Click me
        </button>
        <UserInfor renderContent={this.renderTitle} data={user} />
      </div>
    );
  }
}
