function renderTodoLish(todos) {
    var contentHTML = "";

    // foreach lấy từng phần tử duyệt mảng và trả về cho tham số
    todos.forEach(function (item) {
        var contentTr = `
        <tr>
            <td>${item.id}</td>
            <td>${item.name}</td>
            <td>${item.desc}</td>
            <td>
                <input type="checkbox" check=${item.isComplete ? "checked" : ""}/>
            </td>
            <td>
            <button onclick="removeTodo(${item.id})" class="btn btn-danger">Delete</button>
            </td>
        </tr>
        `;
        contentHTML += contentTr;
    });
    document.getElementById(`tbody-todos`).innerHTML = contentHTML;
}