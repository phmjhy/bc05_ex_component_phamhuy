function tinhDTB(toan, ly, hoa, username) {
    var dtb = (toan + ly + hoa) / 3;
    console.log("Chào", username);
    console.log(`Điểm trung bình của ${username} là`);
    console.log("dtb: ", dtb);
    // chỉ đc return về 1 giá trị
    return dtb;
}

var sv1 = tinhDTB(4, 5, 6, "alice");

var sv2 = tinhDTB(9, 9, 9, "bob");
console.log('sv1: ', sv1);
console.log('sv2: ', sv2);

console.log()