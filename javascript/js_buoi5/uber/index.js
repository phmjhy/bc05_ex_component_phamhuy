const UBER_CAR = "uberCar";
const UBER_SUV = "uberSUV";
const UBER_BLACK = "uberBLACK";

function tinhGiaTienKmDauTien(car) {
    var giaTien;
    if (car== UBER_CAR) {
        giaTien = 8000;
    }
    else if (car == UBER_SUV) {
        giaTien = 9000;
    }
    else {
        giaTien = 10000;
    }
    return giaTien;
}  

function tinhGiaTienKmTu1Den19(car) {
    if (car == UBER_CAR) {
        return 7500;
    }
    else if (car ==UBER_SUV) {
        return 8500;
    }
    else {
        return 9500;
    }
}

function tinhGiaTien19KmTroDi(car) {
    switch (car) {
        case car==UBER_CAR: {
            return 7000;
        }
        case car==UBER_SUV: {
            return 8000;
        }
        case car==UBER_BLACK: {
            return 9000;
        }
    }
}
function tinhTienUber () {
    var loaiXe = document.querySelector('input[name="selector"]:checked').value;

    var soKm = document.getElementById("txt-km").value *1;
    var giaTienKmDauTien = tinhGiaTienKmDauTien(loaiXe);
    var giaTienKm1Den19 = tinhGiaTienKmTu1Den19(loaiXe);
    var giaTienTu19KmTroDi = tinhGiaTien19KmTroDi(loaiXe);
    
    
    var tongTien=0;
    if (soKm<=1) {
        tongTien = giaTienKmDauTien * soKm;
    }
    else if (soKm<=19) {
        tongTien = giaTienKmDauTien + (soKm -1) * giaTienKm1Den19;
    }
    else {
        tongTien = giaTienKmDauTien + 18 *giaTienKm1Den19 + (soKm -19) *giaTienTu19KmTroDi;
    }
    document.getElementById("divThanhTien").style.display = "block";
    document.getElementById("divThanhTien").innerHTML = `${tongTien}VND`;
} 

