const DSSV = "DSSV";
var dssv = [];

function luuLocalStorage() {
  let jsonDssv = JSON.stringify(dssv);
  localStorage.setItem(DSSV, jsonDssv);
}

// lấy dữ liệu từ localStorage khi user load trang
// localStorage.getItem : lấy dữu liệu từ local
var dataJson = localStorage.getItem(DSSV);
console.log("dataJson: ", dataJson);
if (dataJson !== null) {
  // convert từ josn thành array
  var svArr = JSON.parse(dataJson);
  // convert data vì dữ liệu không có method
  for (var index = 0; index < svArr.length; index++) {
    var item = svArr[index];
    var sv = new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.toan,
      item.ly,
      item.hoa
    );
    dssv.push(sv);
  }
  renderDssv(dssv);
}

function themSv() {
  var sv = layThongTinTuForm();

  var isValid = true;
  // validate maSv
  isValid =
    kiemTraRong(sv.ma, "spanMaSV", "Mã sinh viên không được để rỗng") &&
    kiemTraTrung(sv.ma, dssv) &&
    kiemTraSo(sv.ma, "spanMaSV", "Mã sinh viên phải là số");
  // validate tenSv
  isValid =
    isValid &
    kiemTraRong(sv.ten, "spanTenSV", "Mã sinh viên không được để rỗng");
  // validate email
  isValid = isValid & kiemTraEmail(sv.email, "spanEmailSV");

  if (isValid) {
    dssv.push(sv);
    // lưu vào localStorage

    luuLocalStorage();

    // render danh sách sinh viên
    renderDssv(dssv);
  }
}

function xoaSv(id) {
  console.log("id: ", id);
  // splice
  var viTri = timKiemViTri(id, dssv);
  console.log("viTri: ", viTri);
  if (viTri !== -1) {
    dssv.splice(viTri, 1);

    luuLocalStorage();
    // render lại giao diện sau khi xoá
    renderDssv(dssv);
  }
}

function suaSv(id) {
  var viTri = timKiemViTri(id, dssv);
  if (viTri == -1) return;

  var data = dssv[viTri];
  console.log("data: ", data);
  showThongTinLenForm(data);
  // ngăn cản user edit id sv
  document.getElementById("txtMaSV").disabled = true;
}

function capNhatSv() {
  var data = layThongTinTuForm();
  console.log("data: ", data);
  var viTri = timKiemViTri(data.ma, dssv);
  if (viTri == -1) return;

  dssv[viTri] = data;
  renderDssv(dssv);
  luuLocalStorage();
  // remove disable
  document.getElementById("txtMaSV").disabled = false;
  resetForm();
}

// CRUD
// crete read update delete

// filter,include
