function SinhVien(maSv, tenSv, email, matKhau, toan, ly, hoa) {
  this.ma = maSv;
  this.ten = tenSv;
  this.email = email;
  this.matKhau = matKhau;
  this.toan = toan;
  this.ly = ly;
  this.hoa = hoa;
  this.tinhDTB = function () {
    //
    //
    return (this.toan * 1 + this.ly * 1 + this.hoa * 1) / 3;
  };
}
function Cat(catName, catAge) {
  this.username = catName;
  this.number = catAge;
  this.talk = function () {
    console.log("i am a good cat,", this.username);
  };
}
var cat3 = new Cat("tom", 2);
