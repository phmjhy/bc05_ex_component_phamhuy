function Cat(catName, catAge) {
  this.username = catName;
  this.number = catAge;
  this.talk = function () {
    console.log("i am a good cat,", this.username);
  };
}

var cat1 = {
  name: "alice",
  age: 2,
};
console.log("cat1: ", cat1);
var cat2 = {
  ten: "bob",
  tuoi: 1,
};
console.log("cat2: ", cat2);

var cat3 = new Cat("tom", 2);
var cat4 = new Cat("tomy", 20);
console.log("cat4: ", cat4);
console.log("cat3: ", cat3.username);
cat3.talk();
