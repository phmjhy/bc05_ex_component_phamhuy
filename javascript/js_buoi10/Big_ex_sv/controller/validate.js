function kiemTraTrung(id,dssv) {
    let index= timKiemViTri(id,dssv);
    if (index !==-1) {
        showMessageErr("spanMaSV", "Mã Sinh Viên đã tồn tại ") 
        return false
    } else {
        showMessageErr("spanMaSV", ``)
        return true;
    }
}

function kiemTraRong(userInput,idErr,message) {
    if (userInput.length == 0) {
        showMessageErr(idErr,message)
        return false;
    } else {
        showMessageErr(idErr,``)
        return true;
    }
}