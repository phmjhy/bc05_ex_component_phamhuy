function layThongTinTuForm() {
  var maSv = document.getElementById('txtMaSV').value;
  var tenSv = document.getElementById('txtTenSV').value;
  var email = document.getElementById('txtEmail').value;
  var matKhau = document.getElementById('txtPass').value;
  var diemToan = document.getElementById('txtDiemToan').value;
  var diemLy = document.getElementById('txtDiemLy').value;
  var diemHoa = document.getElementById('txtDiemHoa').value;

  var sv = new SinhVien(maSv,tenSv,email,matKhau,diemToan,diemLy,diemHoa)
  return sv;
}

function renderDssv(svArr) {
    var contentHTML = "";

    for (var index = 0; index < svArr.length; index++) {
    var currentSv = svArr[index];
    
    var contentTr=`
    <tr>
      <td>${currentSv.ma}</td>
      <td>${currentSv.ten}</td>
      <td>${currentSv.email}</td>
      <td>${currentSv.tinhDTB().toFixed(1)}</td>
      <td><button class="btn btn-danger">Xóa</button></td>
      <td><button class="btn btn-warning">Sửa</button></td>
    </tr>
    `;
    contentHTML += contentTr;
  }
  document.getElementById(`tbodySinhVien`).innerHTML = contentHTML;
}